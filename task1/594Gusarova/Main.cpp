#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>

static float Dot(glm::vec2 a, glm::vec2 b){
    return a.x * b.x + a.y * b.y;
}

static glm::vec2 Distance(glm::vec2 a, glm::vec2 b){
    return glm::vec2(a.x - b.x, a.y - b.y);
}


static float QunticCurve(float t){
    return t * t * t * (t * (t * 6 - 15) + 10);
}

static float Lerp(float a, float b, float t){
    return a + (b - a) * t;
}

static glm::vec3 Distance3(glm::vec3 a, glm::vec3 b){
    return glm::vec3(b.x - a.x, b.y - a.y, b.z - a.z);
}

static glm::vec3 Normal(glm::vec3 a, glm::vec3 b, glm::vec3 c){
    glm::vec3 first = Distance3(b, a);
    glm::vec3 second = Distance3(c, a);
    glm::vec3 normal = glm::vec3(first.y * second.z - first.z * second.y,
            first.z * second.x - first.x * second.z,
            first.x * second.y - first.y * second.x);
    float nl = pow((pow(normal.x, 2) + pow(normal.y, 2) + pow(normal.z, 2)), 0.5);
    return glm::vec3(normal.x / nl, normal.y / nl, normal.z / nl);

}


glm::vec3 CalculateHeight(glm::vec4 a, glm::vec4 b, glm::vec4 c, glm::vec4 d, glm::vec2 p, float ampl){
    p = glm::vec2(p.x + a.x, p.y + a.y);
    float topLeft = Dot(Distance(p, glm::vec2(a.x, a.y)), glm::vec2(a.z, a.w));
    float topRight = Dot(Distance(p, glm::vec2(b.x, b.y)), glm::vec2(b.z, b.w));
    float bottomLeft = Dot(Distance(p, glm::vec2(c.x, c.y)), glm::vec2(c.z, c.w));
    float bottomRight = Dot(Distance(p, glm::vec2(d.x, d.y)), glm::vec2(d.z, d.w));

    float t_y = QunticCurve((p.x - a.x) / (c.x - a.x));
    float t_x = QunticCurve((p.y - a.y) / (b.y - a.y));

    float top = Lerp(topLeft, topRight, t_x);
    float bottom = Lerp(bottomLeft, bottomRight, t_x);
    float height = Lerp(top, bottom, t_y);

    return glm::vec3(p.x, p.y, height * ampl);
}

/**
 Создает модель рельефа, размера size, на quads точках с scale количеством ячеек для точки и амплитудой ampl
*/
MeshPtr makeRelief(float size, int quads, int scale, float ampl)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    srand(time(NULL));

    float shift = 2 * size / quads;
    float shift_on_scale = shift / scale;
    float current_x, current_y;

    std::vector<glm::vec4> points;
    int pos_x, pos_y, pos_scale_x, pos_scale_y;


    for (pos_x=0; pos_x < quads+1; pos_x++)
        for (pos_y=0; pos_y < quads+1; pos_y++){
            current_x = -size + pos_x * shift;
            current_y = -size + pos_y * shift;

            int v = (uint) (rand()) % 4;
            switch (v) {
                case 0: {points.push_back(glm::vec4(current_x, current_y, 1.0, 0.0)); break; }
                case 1: {points.push_back(glm::vec4(current_x, current_y, -1.0, 0.0)); break; }
                case 2: {points.push_back(glm::vec4(current_x, current_y, 0.0, 1.0)); break; }
                case 3: {points.push_back(glm::vec4(current_x, current_y, 0.0, -1.0)); break; }
            }
        }

    glm::vec4 a, b, c, d;
    for (pos_x=0; pos_x < quads; pos_x++)
        for (pos_y=0; pos_y < quads; pos_y++) {
            a = points[pos_x * (quads+1) + pos_y];
            b = points[pos_x * (quads+1) + pos_y + 1];
            c = points[(pos_x+1) * (quads+1) + pos_y];
            d = points[(pos_x+1) * (quads+1) + pos_y + 1];


            for (pos_scale_x = 0; pos_scale_x < scale; pos_scale_x++)
                for (pos_scale_y = 0; pos_scale_y < scale; pos_scale_y++) {
                    glm::vec2 a_, b_, c_, d_;
                    a_ = glm::vec2(pos_scale_x * shift_on_scale, pos_scale_y * shift_on_scale);
                    b_ = glm::vec2(pos_scale_x * shift_on_scale, (pos_scale_y+1) * shift_on_scale);
                    c_ = glm::vec2((pos_scale_x+1) * shift_on_scale, pos_scale_y * shift_on_scale);
                    d_ = glm::vec2((pos_scale_x+1) * shift_on_scale, (pos_scale_y+1) * shift_on_scale);

                    glm::vec3 a_h, b_h, c_h, d_h;
                    a_h = CalculateHeight(a, b, c, d, a_, ampl);
                    b_h = CalculateHeight(a, b, c, d, b_, ampl);
                    c_h = CalculateHeight(a, b, c, d, c_, ampl);
                    d_h = CalculateHeight(a, b, c, d, d_, ampl);


                    vertices.push_back(a_h);
                    vertices.push_back(b_h);
                    vertices.push_back(c_h);

                    glm::vec3 normal = Normal(a_h, c_h, b_h);

                    normals.push_back(normal);
                    normals.push_back(normal);
                    normals.push_back(normal);

                    vertices.push_back(b_h);
                    vertices.push_back(c_h);
                    vertices.push_back(d_h);

                    normal = Normal(d_h, b_h, c_h);


                    normals.push_back(normal);
                    normals.push_back(normal);
                    normals.push_back(normal);
                }
        }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());


    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Cube is created with " << vertices.size() << " vertices\n";

    return mesh;
}


class SampleApplication : public Application {
public:
    MeshPtr _cube;
    ShaderProgramPtr _shader;

    void makeScene() override {
        Application::makeScene();

        _cube = makeRelief(2.0f, 8, 24, 0.75);

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("594GusarovaData1/shaderHeight.vert", "594GusarovaData1/shader.frag");
    }



    void draw() override {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);


        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        {
            _shader->setMat4Uniform("modelMatrix", _cube->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(
                    glm::inverse(glm::mat3(_camera.viewMatrix * _cube->modelMatrix()))));
            _cube->draw();
        }
    }


};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}