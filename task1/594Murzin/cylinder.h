#ifndef PROJECT_CYLINDER_H
#define PROJECT_CYLINDER_H

#include <iostream>

using std::sin;
using std::cos;

MeshPtr makeCone(float height, float radius, size_t n) {
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texcoords;

	for (size_t i = 0; i < n; ++i) {
		// if (i % 2 == 0) continue;
		float theta1 = 2 * glm::pi<float>() * i / n;
		float theta2 = 2 * glm::pi<float>() * (i + 1) / n;
		float theta = (theta1 + theta2) / 2;

		vertices.emplace_back(0, 0, height);
		vertices.emplace_back(radius * cos(theta1), radius * sin(theta1), 0);
		vertices.emplace_back(radius * cos(theta2), radius * sin(theta2), 0);

		normals.emplace_back(cos(theta), sin(theta), radius / height);
		normals.emplace_back(cos(theta1), sin(theta1), radius / height);
		normals.emplace_back(cos(theta2), sin(theta2), radius / height);
	}

	//----------------------------------------

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	// DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	// buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	// mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	std::cout << "Cone is created with " << vertices.size() << " vertices\n";

	return mesh;
}

#endif
