#include "./common/Application.hpp"
#include "./common/Mesh.hpp"
#include "./common/ShaderProgram.hpp"

#include "revolution.hpp"

#include <iostream>
#include <vector>


class SampleApplication : public Application
{
public:
    MeshPtr _surface;

    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _surface = generateSurface(3.0, 1, N);
        _surface->setModelMatrix(glm::mat4(1.0f));

        _shader = std::make_shared<ShaderProgram>("./595IslamovData1/shader.vert", "./595IslamovData1/shader.frag");
    }


    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _surface->draw();


    }

    void updateSurface(int dN) {
        N += dN;
        _surface = generateSurface(3.0, 1, N);
        _surface->setModelMatrix(glm::mat4(1.0f));
        draw();
    }

    void handleKey(int key, int scancode, int action, int mods) override {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS ) {
                updateSurface(-2);
            } else if (key == GLFW_KEY_EQUAL) {
                updateSurface(2);
            }
        }
    }
private:
    unsigned int N = 20;
};

int main()
{
    SampleApplication app;
    app.start();
    return 0;
}