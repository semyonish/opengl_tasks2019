set(SRC_FILES
        common/Application.cpp
        common/DebugOutput.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        common/Texture.cpp
        surface.cpp
        )

set(HEADER_FILES
        common/Application.hpp
        common/DebugOutput.h
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        common/Texture.hpp
        common/LightInfo.hpp
        )

#MAKE_SAMPLE(surface)
MAKE_OPENGL_TASK(597Ishkhanyan 2 "${SRC_FILES}")

set(SHADER_FILES
        597IshkhanyanData2/marker.frag
        597IshkhanyanData2/marker.vert
        597IshkhanyanData2/texture.frag
        597IshkhanyanData2/texture.vert
        )

source_group("Shaders" FILES
        ${SHADER_FILES}
        )

#COPY_RESOURCE(597IshkhanyanData2)
#add_dependencies(surface 597IshkhanyanData2)

install(DIRECTORY ${PROJECT_SOURCE_DIR}/597Ishkhanyan/597IshkhanyanData2 DESTINATION ${CMAKE_INSTALL_PREFIX})
